<?php

namespace Tests\Web\Http;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class AccessTest extends TestCase
{

    public function testForStatusOkOnMainPage()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }

    public function testRandomRequestsShouldReturn404()
    {
        $response = $this->get('/jsd689us67sh678?_SESSION=47284');

        $response->assertStatus(404);
    }
}
