<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'Web\PostController@getPosts')->name('front');

Route::group(['prefix' => 'dashboard'], function ($request) {

    Route::post('/register', 'Web\AuthController@register');
    Route::get('/register', 'Web\AuthController@registerPage');
    Route::post('/login', 'Web\AuthController@login');
    Route::get('/', 'Web\AuthController@loginPage');
    Route::get('/login', 'Web\AuthController@loginPage')->name('login');
    Route::get('/logout', 'Web\AuthController@logout');

    /**
     * Authenticated admin routes
     */

    Route::group(['middleware' => 'auth'], function ($request){

        Route::get('/home', 'Admin\PostController@getPosts')->name('home');

        /**
         * Post pages
         */
        Route::group(['prefix' => 'posts'], function(){
            Route::get('/view/{post_id}', 'Admin\PostController@viewPost')->name('viewPost');
            Route::post('/update/{post_id}', 'Admin\PostController@updatePost')->name('updatePost');
            Route::post('/add', 'Admin\PostController@addPost')->name('addPost');
            Route::get('/new', 'Admin\PostController@newPostPage')->name('newPostPage');
        });

    });

    /**
     * End of Admin
     */

});
