@extends('web.layouts.plain')

@section('content')
    <div class="right_col" role="main">
        <div class="page-title bg-white col-xs-12">
            <div class="title_left">
                <h3 class="col-xs-12">{{date('F')}} {{date('Y')}}</h3>
            </div>
            <div class="title_right">
                <ul class="breadcrumb pull-right">
                    <li><a href="">Add Data</a></li>
                    <li><a href="/dashboard">Dashboard</a></li>
                    <li><a href="/dashboard/logout">Log Out</a></li>
                </ul>
            </div>
        </div>
        <div class="x_panel">
            <div class="page-title page-title-inner">
                <div class="title_left">
                    <h3> Add Data </h3>
                </div>
                <div class="title_right"></div>
            </div>
            <div class="x_content">
                <form role="form" class="form-inline" action="{{ route('addPost') }}" method="post" enctype="multipart/form-data">
                    <div class="col-sm-12 height1 border-bottom"></div>
                    <h4 class="grey-font col-sm-12">Details</h4>
                    <div class="col-sm-6 col-xs-12">
                        <div class="form-group">
                            <label for="title">Title</label>
                            <input type="text" class="form-control" id="title" name="title" >
                        </div>
                        <div class="form-group">
                            <label for="body">Body</label>
                            <textarea class="form-control col-xs-12" id="body" name="body" ></textarea>
                        </div>
                        <div class="form-group">
                            <label for="csv" class="col-md-4 control-label">CSV File</label>
                            <div class="col-md-6">
                                <input id="csv" type="file" class="form-control-file space" name="csv" required>
                            </div>
                        </div>
                    </div>
                    <button class="btn btn-purple-in pull-right margin-left submit-btn" type="submit">Upload CSV</button>
                    <input type="hidden" name="_token" value="{{ Session::token() }}">
                </form>
                <div class="col-sm-12 height1 border-bottom"></div>
            </div>
        </div>
    </div>
@endsection
