@extends('web.layouts.plain')

@section('content')
    <div class="col" role="main">
        <div class="page-title bg-white col-xs-12">
            <div class="title_left">
                <h3 class="col-xs-12">Welcome {{Auth::user()->username}}, today is {{date('D dS')}} {{date('F')}} {{date('Y')}}</h3>
            </div>
            <div class="title_right">
                <ul class="breadcrumb pull-right">
                    <li><a href="/" target="_blank">Site</a></li>
                    <li><a href="/dashboard/home">All Postss</a></li>
                    <li><a href="/dashboard/logout">Log Out</a></li>
                </ul>
            </div>
        </div>
        <div class="col-xs-12">
            <div class="page-title page-title-inner">
                <div class="title_left">
                    <h3> All Posts</h3>
                    <form class="col-md-7 col-sm-7 col-xs-12 form-group pull-left top_search"  action="{{ route('home') }}" method="get">
                        <div class="input-group">
                            <input type="text" name="s" class="form-control" placeholder="Search by title or body">
                            <span class="input-group-btn">
                            <button class="btn btn-default" type="submit">Go!</button>
                                {{--<input type="hidden" name="_token" value="{{ Session::token() }}">--}}
                            </span>
                            <a href="/dashboard/posts/new" class="btn bt col-xs-12 white btn-purple-in ml-10" >New Post</a>
                        </div>
                    </form>
                </div>
                <div class="title_right">
                </div>
            </div>
            <div class="x_content">
                @if(isset($noResults))
                    <section class="text-center">
                        <h3>No results found for "{{ $noResults }}".</h3>
                    </section>
                @else
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>Id </th>
                        <th>Title </th>
                        <th>First Name </th>
                        <th>Initial </th>
                        <th>Last Name </th>
                        <th width="100"></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($posts as $post)
                        <tr>
                            <td class="middle"><a href="{{ route('viewPost', $post->id) }}"> {{ $post->id }} </a></td>
                            <td class="middle"><a href="{{ route('viewPost', $post->id) }}" class="user-profile"> {{ $post->title }} </a></td>
                            <td class="middle"><a href="{{ route('viewPost', $post->id) }}">{{ $post->first_name }}</a></td>
                            <td class="middle"><a href="{{ route('viewPost', $post->id) }}">{{ $post->initial }}</a></td>
                            <td class="middle"><a href="{{ route('viewPost', $post->id) }}">{{ $post->last_name }}</a></td>
                            <td class="middle">{{ $post->user->name }}</td>
                            <td class="base"><a href="{{ route('viewPost', $post->id) }}" class="btn btn-purple-in">View Post</a></td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                @endif
            </div>
        </div>
        <!-- pagination -->
        <div class="col-sm-12">
            {{ $posts->links() }}
        </div>
    </div>
@endsection
