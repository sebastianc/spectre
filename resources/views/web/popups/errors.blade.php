
<div class="error_message{{ Session::has('error_message') ? ' hide-after-delay' : '' }}">{{ Session::pull('error_message') }}
    {{ Session::has('attempts_remaining') ? ' You have ' . Session::pull('attempts_remaining') . ' attempts remaining.' : '' }}
</div>
<div class="success_message{{ Session::has('success_message') ? ' hide-after-delay' : '' }}">{{ Session::pull('success_message')  }}</div>
