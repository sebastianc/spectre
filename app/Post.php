<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Sofa\Eloquence\Eloquence;

class Post extends Model
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'body', 'user_id', 'created_at', 'updated_at', 'deleted_at',
    ];

    protected $dates = ['deleted_at', 'created_at', 'updated_at'];

    /**
     * Custom to array function
     * @return array
     */
    public function toArray(){
        $array = parent::toArray();
        return $array;
    }

    use Eloquence;

    /**
     * Searchable rules.
     *
     * @var array
     */
    protected $searchableColumns = [
        'title' => 200,
        'body' => 250,
    ];

    /**
     * The user that created the Post
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

}
