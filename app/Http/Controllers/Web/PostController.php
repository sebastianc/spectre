<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Post;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PostController extends Controller
{

    public function getPosts(Request $request) {

        $posts = Post::whereNull('deleted_at');
        if(isset($request['s'])){
            $searchText = $request['s'];
            $posts = $posts->where('title', 'LIKE', '%'.$searchText.'%')->orWhere('body', 'LIKE', '%'.$searchText.'%')
                ->orderBy('created_at', 'desc')->paginate(15);
            if($posts->count() === 0 ){
                return view('web.pages.posts', ['posts' => $posts, 'noResults' => $searchText]);
            }
            else{
                return view('web.pages.posts', ['posts' => $posts]);
            }
        }
        else{
            $posts = $posts->orderBy('created_at', 'desc')->get();
            return view('web.pages.posts', ['posts' => $posts]);
        }

    }
}
