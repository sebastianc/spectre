<?php

namespace App\Http\Controllers\Web;


use App\Http\Controllers\Controller;
use App\PasswordReset;
use Illuminate\Http\Request;

class PasswordResetController extends Controller
{
    public function view(Request $request)
    {
        $reset = PasswordReset
            ::where('token', $request->input('token'))
            ->where('used', false)
            ->firstOrFail();

        return view('web.password.reset', [
            'reset' => $reset
        ]);
    }

    public function save(Request $request)
    {
        $this->validate($request, [
            'token' => 'required',
            'password' => 'required|confirmed',
        ]);

        $reset = PasswordReset
            ::where('token', $request->input('token'))
            ->where('used', false)
            ->firstOrFail();

        $valid = $reset->user->usePasswordReset($reset->token, $request->input('password'));

        if (!$valid) {
            abort(400);
        }

        $reset->user->save();
        
        return view('web.password.updated');
    }
}