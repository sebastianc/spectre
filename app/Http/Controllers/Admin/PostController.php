<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Post;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;

class PostController extends Controller
{
    public function getPosts(Request $request) {
        $posts = Post::whereNull('deleted_at')->orderBy('id', 'asc');
        if(isset($request['s'])){
            $searchText = $request['s'];
            $posts = $posts->where('title', 'LIKE', '%'.$searchText.'%')
                ->orWhere('first_name', 'LIKE', '%'.$searchText.'%')
                ->orWhere('last_name', 'LIKE', '%'.$searchText.'%')
                ->orderBy('created_at', 'desc')->paginate(15);
            if($posts->count() === 0 ){
                return view('web.admin.posts', ['posts' => $posts, 'noResults' => $searchText]);
            }
            else{
                return view('web.admin.posts', ['posts' => $posts]);
            }
        }
        else{
            $posts = $posts->orderBy('id', 'desc')->paginate(15);
            return view('web.admin.posts', ['posts' => $posts]);
        }
    }

    public function viewPost($postId) {
        $post = Post::where('id', $postId)->whereNull('deleted_at')->first();

        if(isset($post->id)){
            return view('web.admin.viewPost', ['post' => $post]);
        } else {
            return redirect()->route('home')->with(['error_message' => 'Post not found or deleted.']);
        }

    }

    public function updatePost(Request $request, $postId){
        $post = Post::find($postId);

        if($post->user_id == Auth::user()->id){
            if (isset($request['title']) && isset($request['body'])){
                $post->title = $request['title'];
                $post->body = $request['body'];
                //$user = User::where('id',$request['user_id'])->first();
                $post->save();
            }

            //Possible future implementation for post images

            if( $request->hasFile('post_image') ) {
                $imageName = $request->file('post_image')->getClientOriginalName();
                $path = base_path() . '/public/uploads/images/';
                $request->file('post_image')->move($path , $imageName);
                $post->post_image = Image::make($path.$imageName);
                $post->save();
            }

            if (isset($request['delete'])){
                $post->delete();
                return redirect()->route('home')->with(['success_message' => 'Post successfully deleted.']);
            }

            return redirect()->route('viewPost', $postId)->with(['success_message' => 'Post successfully updated.']);
        } else {
            return redirect()->route('viewPost', $postId)->with(['error_message' => 'You are not the author of this post.']);
        }
    }

    public function addPost(Request $request){

        $validation = Validator::make($request->all(), [
            'csv' => 'required',
        ]);

        if ($validation->fails()) {
            $responseArray = array(
                'success' => false,
                'message' => 'Request has failed validation test ',
                'response_code' => 422,
                'data' => $validation->errors()
            );
            return response()->json($responseArray, $responseArray['response_code']);
        }

        if ($request->hasFile('csv')) {

            $csv = File::get($request->file('csv'));

            foreach(explode("\n", $csv) as $line) {

                if (stripos($line, ' ') !== false) {

                    if (stripos($line, 'and') !== false) {

                        $postFirst = new Post;

                        $line = explode(' ', $line);

                        if (!in_array(strtolower($line[0]), ['mr', 'mrs', 'miss', 'ms', 'dr'])) {
                            $postFirst->title = '';
                        } else {
                            $postFirst->title = $line[0];
                        }

                        $postFirst->last_name = end($line);

                        $postFirst->body = '';

                        $postFirst->user_id = Auth::user()->id;
                        $postFirst->save();


                        $postSecond = new Post;

                        if (!in_array(strtolower($line[0]), ['mr', 'mrs', 'miss', 'ms', 'dr'])) {
                            $postSecond->title = '';
                        } else {
                            $postSecond->title = $line[2];
                        }

                        $postSecond->last_name = end($line);

                        $postSecond->body = '';

                        $postSecond->user_id = Auth::user()->id;
                        $postSecond->save();

                    } else {

                        $post = new Post;

                        $line = explode(' ', $line);

                        if (!in_array(strtolower($line[0]), ['mr', 'mrs', 'miss', 'ms', 'dr'])) {
                            $post->title = '';
                        } else {
                            $post->title = $line[0];
                        }

                        if (strlen($line[1] < 3) && (stripos($line[1], '.') === false)) {

                            $post->first_name = $line[1];
                            $post->initial = '';
                        } else {
                            $post->initial = $line[1];
                            $post->first_name = '';
                        }

                        $post->last_name = end($line);

                        $post->body = '';

                        $post->user_id = Auth::user()->id;
                        $post->save();
                    }
                }
            }

            return redirect()->route('home')->with(['success_message' => 'Data successfully uploaded.']);

        } else {
            return redirect()->route('home')->with(['failed_message' => 'No data to upload.']);
        }

    }

    public function newPostPage(Request $request)
    {
        return view('web.admin.newPost');
    }

}
