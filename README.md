<h1>Overview</h1>
<p align="center">
Interactive API for submitting CSV content based on Laravel 5.8.18.
</p>
<h1>Various Project Settings</h1>
<p align="center">
git clone git@gitlab.com:sebastianc/spectre.git
</p>
<p align="center">
composer update
</p>
<h2>Env Settings</h2>
<p align="center">
Run:
</p>
<p align="center">
php artisan vendor:publish --provider="Tymon\JWTAuth\Providers\LaravelServiceProvider"
</p>
<p align="center">
php artisan jwt:secret
</p>
<p align="center">
Example output:
</p>
<p align="center">
Application key [base64:123456789BVP1xCQtbI8vrpwqNFmq5ONw9jKddB42HA=] set successfully.
</p>
<p align="center">
DB_CONNECTION=sqlite
</p>
<p align="center">
DB_DATABASE=/absolute/path/to/database.sqlite
</p>
<h2>Sqlite driver</h2>
<p align="center">
Driver that allows you to use PHP with SQLite:
</p>
<p align="center">
sudo apt-get install php7.2-sqlite3
</p>
<p align="center">
For any issues on Linux with the driver install run:
</p>
<p align="center">
sudo apt-get update
</p>
<p align="center">
Vagrant default password is vagrant
</p>
<p align="center">
</p>
<p align="center">
Then restart Apache/Nginx
</p>
<h2>Migrations</h2>
<p align="center">
Run this once for Sqlite before running migrations:
</p>
<p align="center">
php artisan migrate:install --env=local
</p>
<p align="center">
</p>
<p align="center">
Then always migrate with:
</p>
<p align="center">
php artisan migrate --env=local
</p>
<h2>Mail Provider</h2>
<p align="center">
Set up in .env
</p>
<h2>Local Dev Server</h2>
<p align="center">
If you have PHP installed locally and you would like to use PHP's built-in development server, you may use the serve Artisan command. This command will start a development server at http://localhost:8000
</p>
<p align="center">
php artisan serve
</p>
<h2>Permissions</h2>
<p align="center">
You may need to configure some permissions for non-Homestead installations. Directories within the <b>storage</b> and the <b>bootstrap/cache</b> directories <b>must</b> be writable by your web server.
</p>
<h2>Unit Tests:</h2>
<p align="center">
vendor/bin/phpunit tests/Unit/Http/AccessTest.php
</p>
<p align="center">
vendor/bin/phpunit tests/Unit/Models/UserTest.php
</p>
<p align="center">
vendor/bin/phpunit tests/Unit/Models/PostTest.php
</p>
<p align="center">
vendor/bin/phpunit --filter testPasswordIsHashed TestCase tests/Unit/Models/UserTest.php
</p>
<p align="center">
vendor/bin/phpunit --filter testEloquenceSmartSearchIsWorking TestCase tests/Unit/Models/UserTest.php
</p>
<p align="center">
vendor/bin/phpunit --filter testCreatePhoneVerificationCode TestCase tests/Unit/Models/UserTest.php
</p>
<p align="center">
vendor/bin/phpunit --filter testWhenUserTriesToVerifyPhone TestCase tests/Unit/Models/UserTest.php
</p>
<p align="center">
vendor/bin/phpunit --filter testWhenUserVerifiesPhoneWithAnInvalidCode TestCase tests/Unit/Models/UserTest.php
</p>
<p align="center">
vendor/bin/phpunit --filter testWhenAPasswordResetRequestIsCreated TestCase tests/Unit/Models/UserTest.php
</p>
<p align="center">
vendor/bin/phpunit --filter testWhenUserUtilizesThePasswordResetRequest TestCase tests/Unit/Models/UserTest.php
</p>
<h2>About</h2>
<p align="center">For any issues contact me at explorehumanity@gmail.com</p>

<h2>Project Framework</h2>
<p align="center"><img src="https://laravel.com/assets/img/components/logo-laravel.svg"></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>

## About Laravel

Laravel is a web application framework with expressive, elegant syntax. We believe development must be an enjoyable and creative experience to be truly fulfilling. Laravel attempts to take the pain out of development by easing common tasks used in the majority of web projects, such as:

- [Simple, fast routing engine](https://laravel.com/docs/routing).
- [Powerful dependency injection container](https://laravel.com/docs/container).
- Multiple back-ends for [session](https://laravel.com/docs/session) and [cache](https://laravel.com/docs/cache) storage.
- Expressive, intuitive [database ORM](https://laravel.com/docs/eloquent).
- Database agnostic [schema migrations](https://laravel.com/docs/migrations).
- [Robust background job processing](https://laravel.com/docs/queues).
- [Real-time event broadcasting](https://laravel.com/docs/broadcasting).

Laravel is accessible, yet powerful, providing tools needed for large, robust applications.

## Learning Laravel

Laravel has the most extensive and thorough [documentation](https://laravel.com/docs) and video tutorial library of any modern web application framework, making it a breeze to get started learning the framework.

If you're not in the mood to read, [Laracasts](https://laracasts.com) contains over 1100 video tutorials on a range of topics including Laravel, modern PHP, unit testing, JavaScript, and more. Boost the skill level of yourself and your entire team by digging into our comprehensive video library.

## Laravel Sponsors

We would like to extend our thanks to the following sponsors for helping fund on-going Laravel development. If you are interested in becoming a sponsor, please visit the Laravel [Patreon page](http://patreon.com/taylorotwell):

- **[Vehikl](https://vehikl.com/)**
- **[Tighten Co.](https://tighten.co)**
- **[British Software Development](https://www.britishsoftware.co)**
- [Fragrantica](https://www.fragrantica.com)
- [SOFTonSOFA](https://softonsofa.com/)
- [User10](https://user10.com)
- [Soumettre.fr](https://soumettre.fr/)
- [CodeBrisk](https://codebrisk.com)
- [1Forge](https://1forge.com)
- [TECPRESSO](https://tecpresso.co.jp/)
- [Pulse Storm](http://www.pulsestorm.net/)
- [Runtime Converter](http://runtimeconverter.com/)
- [WebL'Agence](https://weblagence.com/)

## Contributing

Thank you for considering contributing to the Laravel framework! The contribution guide can be found in the [Laravel documentation](http://laravel.com/docs/contributions).

## Security Vulnerabilities

If you discover a security vulnerability within Laravel, please send an e-mail to Taylor Otwell via [taylor@laravel.com](mailto:taylor@laravel.com). All security vulnerabilities will be promptly addressed.

## License

The Laravel framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT).